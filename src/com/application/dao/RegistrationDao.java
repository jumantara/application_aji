package com.application.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.applicataion.main.Registration;
import com.application.model.RegistrationMod;
import com.application.util.Constant;
import com.application.util.DataConnection;
import com.application.util.ErrorHandle;

public class RegistrationDao implements BaseDao {
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public List getAllData() {
		List listRegistration = new ArrayList();
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "select * from tbl_customer";
			PreparedStatement ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				RegistrationMod registration = new RegistrationMod();
				registration.setId_cust(rs.getLong("id_cust"));
				registration.setId_user(rs.getLong("id_user"));
				registration.setUsername(rs.getString("username"));
				registration.setHandphone(rs.getString("handphone"));
				registration.setAlamat(rs.getString("alamat"));
				registration.setPassword(rs.getString("password"));
				registration.setLevel(rs.getString("level"));
				
				listRegistration.add(registration);
			}
			DataConnection.close(ps, rs);
			DataConnection.end(connection);
		}catch (Exception e) {
			ErrorHandle.handleException(Constant.ERROR_GET_DATA, e);
		}
		return listRegistration;
	}

	@Override
	public Object getDataById(Long id) {
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		RegistrationMod registration = null;
		try {
			String query = "select * from tbl_customer where id = ?";
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				registration.setUsername(rs.getString("username"));
				registration.setHandphone(rs.getString("handphone"));
				registration.setAlamat(rs.getString("alamat"));
				registration.setPassword(rs.getString("password"));
				registration.setLevel(rs.getString("level"));
				break;
			}
			DataConnection.close(ps, rs);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandle.handleException(Constant.ERROR_GET_DATA, e);
		}
		return registration;
	}
	
	@Override
	public void deleteDataById(Long id) {
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "delete from tbl_customer where id = ?";
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setLong(1, id);
			ps.executeUpdate();
			
			DataConnection.close(ps, null);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandle.handleException(Constant.ERROR_GET_DATA, e);
		}
		
	}
	
	@Override
	public void saveOrUpdate(Object obj) {
		RegistrationMod registrationMod = (RegistrationMod) obj;
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		String query = null;
		PreparedStatement ps = null;
		try {
			if (registrationMod.getId_cust() != null) {
				System.out.println("perintahny kesini");
				query = "update tbl_customer set id_cust = ?, username = ?, handphone = ?, alamat = ?, password = ?, level = ?, id_user = ? where id_cust = ?";
				ps = connection.prepareStatement(query);
				ps.setLong(1, registrationMod.getId_cust());
				ps.setLong(2, registrationMod.getId_user());
				ps.setString(3, registrationMod.getUsername());
				ps.setString(4, registrationMod.getHandphone());
				ps.setString(5, registrationMod.getAlamat());
				ps.setString(6, registrationMod.getPassword());
				ps.setString(7, registrationMod.getLevel());
		
			} else {
				System.out.println("sukses");
				query = "insert into tbl_customer (username, handphone, alamat, password, level, id_user) values (?, ?, ?, ?, ?, ?)";
				ps = connection.prepareStatement(query);
				ps.setLong(1, registrationMod.getId_user());
				ps.setString(2, registrationMod.getUsername());
				ps.setString(3, registrationMod.getHandphone());
				ps.setString(4, registrationMod.getAlamat());
				ps.setString(5, registrationMod.getPassword());
				ps.setString(6, registrationMod.getLevel());
			}
			
			
			
			ps.executeUpdate();
			
			DataConnection.close(ps, null);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandle.handleException(Constant.ERROR_GET_DATA, e);
		}
		
	}

	@Override
	public Object getDataByVisibleCode(String code) {
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		RegistrationMod registration = null;
		try {
			String query = "select * from tbl_customer where id_cust = ?";
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setString(1, code);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				registration = new RegistrationMod();
				registration.setId_cust(rs.getLong("id_cust"));
				registration.setId_user(rs.getLong("id_user"));
				registration.setUsername(rs.getString("username"));
				registration.setHandphone(rs.getString("handphone"));
				registration.setAlamat(rs.getString("alamat"));
				registration.setPassword(rs.getString("password"));
				registration.setLevel(rs.getString("level"));
			}
			
			DataConnection.close(ps, rs);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandle.handleException(Constant.ERROR_GET_DATA, e);
		}
		return registration;
	}
	
}
