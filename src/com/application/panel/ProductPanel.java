package com.application.panel;

import java.awt.Color;

import javax.swing.JPanel;

import com.application.util.Constant;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

public class ProductPanel extends JPanel {
	private JTextField txtId;
	private JTextField txtName;
	private JTextField txtPrice;
	private JTextField txtStock;
	private JButton btnSave;
	private JButton btnEdit;
	private JButton btnDelete;
	private JTable tableProduct;

	/**
	 * Create the panel.
	 */
	public ProductPanel() {
		setBackground(Color.ORANGE);
		setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGHT_PANEL);
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ProductId");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(83, 32, 62, 14);
		add(lblNewLabel);
		
		JLabel lblProductname = new JLabel("ProductName");
		lblProductname.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProductname.setBounds(203, 32, 82, 14);
		add(lblProductname);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrice.setBounds(360, 32, 37, 14);
		add(lblPrice);
		
		JLabel lblStock = new JLabel("Stock");
		lblStock.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblStock.setBounds(487, 32, 37, 14);
		add(lblStock);
		
		txtId = new JTextField();
		txtId.setBounds(69, 57, 86, 20);
		add(txtId);
		txtId.setColumns(10);
		
		txtName = new JTextField();
		txtName.setColumns(10);
		txtName.setBounds(199, 57, 86, 20);
		add(txtName);
		
		txtPrice = new JTextField();
		txtPrice.setColumns(10);
		txtPrice.setBounds(329, 57, 86, 20);
		add(txtPrice);
		
		txtStock = new JTextField();
		txtStock.setColumns(10);
		txtStock.setBounds(460, 57, 86, 20);
		add(txtStock);
		
		btnSave = new JButton("Save");
		btnSave.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSave.setBounds(184, 115, 72, 35);
		add(btnSave);
		
		btnEdit = new JButton("Edit");
		btnEdit.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnEdit.setBounds(272, 115, 72, 35);
		add(btnEdit);
		
		btnDelete = new JButton("Delete");
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnDelete.setBounds(364, 115, 72, 35);
		add(btnDelete);
		
		JScrollPane scrollPaneProduct = new JScrollPane();
		scrollPaneProduct.setBounds(10, 173, 580, 203);
		add(scrollPaneProduct);
		
		tableProduct = new JTable();
		tableProduct.setFont(new Font("Tahoma", Font.BOLD, 11));
		scrollPaneProduct.setViewportView(tableProduct);
		tableProduct.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"ProductId", "ProductName", "Price", "Stock"
			}
		));
		
	}
}
