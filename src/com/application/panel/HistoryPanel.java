package com.application.panel;

import java.awt.Color;

import javax.swing.JPanel;

import com.application.util.Constant;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.Font;

public class HistoryPanel extends JPanel {
	private JTable tableHostory;

	/**
	 * Create the panel.
	 */
	public HistoryPanel() {
		setBackground(Color.YELLOW);
		setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGHT_PANEL);
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 580, 241);
		add(scrollPane);
		
		tableHostory = new JTable();
		scrollPane.setViewportView(tableHostory);
		tableHostory.setToolTipText("HistoryTransaction");
		tableHostory.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"ProductId", "ProductName", "Qty", "Price", "Amount"
			}
		));
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnRefresh.setBounds(181, 299, 82, 35);
		add(btnRefresh);
		
		JButton btnPrint = new JButton("Print");
		btnPrint.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnPrint.setBounds(286, 299, 82, 35);
		add(btnPrint);
	}

}
