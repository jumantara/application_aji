package com.application.panel;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import com.application.util.Constant;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JButton;

public class TransactionPanel extends JPanel {
	private JTextField txtProductId;
	private JTextField txtProduct;
	private JTextField txtPrice;
	private JTextField txtAmount;
	private JTextField txtTotal;
	private JTextField txtPay;
	private JTextField txtChanges;

	/**
	 * Create the panel.
	 */
	public TransactionPanel() {
		setBackground(Color.CYAN);
		setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGHT_PANEL);
		setLayout(null);
		
		JPanel panelTrs = new JPanel();
		panelTrs.setBorder(new TitledBorder(null, "Transaction", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelTrs.setBounds(10, 11, 401, 203);
		add(panelTrs);
		panelTrs.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ProductId");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(10, 28, 58, 14);
		panelTrs.add(lblNewLabel);
		
		JLabel lblProductname = new JLabel("ProductName");
		lblProductname.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProductname.setBounds(10, 53, 76, 14);
		panelTrs.add(lblProductname);
		
		JLabel lblQty = new JLabel("Qty");
		lblQty.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblQty.setBounds(230, 28, 58, 14);
		panelTrs.add(lblQty);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrice.setBounds(230, 53, 58, 14);
		panelTrs.add(lblPrice);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAmount.setBounds(130, 87, 58, 14);
		panelTrs.add(lblAmount);
		
		txtProductId = new JTextField();
		txtProductId.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtProductId.setBounds(94, 25, 106, 20);
		panelTrs.add(txtProductId);
		txtProductId.setColumns(10);
		
		txtProduct = new JTextField();
		txtProduct.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtProduct.setColumns(10);
		txtProduct.setBounds(94, 50, 106, 20);
		panelTrs.add(txtProduct);
		
		txtPrice = new JTextField();
		txtPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtPrice.setColumns(10);
		txtPrice.setBounds(285, 50, 106, 20);
		panelTrs.add(txtPrice);
		
		txtAmount = new JTextField();
		txtAmount.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtAmount.setColumns(10);
		txtAmount.setBounds(185, 84, 106, 20);
		panelTrs.add(txtAmount);
		
		JSpinner spinnerQty = new JSpinner();
		spinnerQty.setFont(new Font("Tahoma", Font.BOLD, 11));
		spinnerQty.setBounds(285, 25, 106, 20);
		panelTrs.add(spinnerQty);
		
		JButton btnSave = new JButton("Save");
		btnSave.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSave.setBounds(160, 150, 76, 31);
		panelTrs.add(btnSave);
		
		JPanel panelBayar = new JPanel();
		panelBayar.setBounds(10, 225, 401, 164);
		add(panelBayar);
		panelBayar.setLayout(null);
		
		JLabel lblTotal = new JLabel("Total");
		lblTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTotal.setBounds(42, 25, 35, 14);
		panelBayar.add(lblTotal);
		
		JLabel lblPay = new JLabel("Pay");
		lblPay.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPay.setBounds(183, 25, 30, 14);
		panelBayar.add(lblPay);
		
		JLabel lblMoneyChanges = new JLabel("Money Changes");
		lblMoneyChanges.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMoneyChanges.setBounds(294, 25, 97, 14);
		panelBayar.add(lblMoneyChanges);
		
		txtTotal = new JTextField();
		txtTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtTotal.setColumns(10);
		txtTotal.setBounds(10, 50, 106, 20);
		panelBayar.add(txtTotal);
		
		txtPay = new JTextField();
		txtPay.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtPay.setColumns(10);
		txtPay.setBounds(142, 50, 106, 20);
		panelBayar.add(txtPay);
		
		txtChanges = new JTextField();
		txtChanges.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtChanges.setColumns(10);
		txtChanges.setBounds(285, 50, 106, 20);
		panelBayar.add(txtChanges);
		
		JButton btnBill = new JButton("Bill");
		btnBill.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnBill.setBounds(158, 106, 76, 31);
		panelBayar.add(btnBill);
		
		JPanel panelPrint = new JPanel();
		panelPrint.setBounds(421, 11, 169, 277);
		add(panelPrint);
		
		JButton btnPrint = new JButton("Print");
		btnPrint.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnPrint.setBounds(471, 328, 76, 31);
		add(btnPrint);
	}
}
