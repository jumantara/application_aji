package com.application.util;

import javax.swing.JOptionPane;

public class ErrorHandle {
	public static void handleException(String error, Exception e) {
		try {
			JOptionPane.showConfirmDialog(null, error, Constant.TITTLE_APP, JOptionPane.ERROR_MESSAGE);
			System.out.println("Detail Error: " + e);
		}catch (Exception ex) {
			System.out.println("Load image failed: " + ex);
		}
	}
}
