package com.application.util;

import java.util.ArrayList;
import java.util.List;

public class Constant {
	public static final Integer WIDTH_PANEL = 623;
	public static final Integer HEIGHT_PANEL = 439;
	public static final String TITTLE_APP = "Toko Xiaji";
	public static final String[] LEVEL_TYPE = {"- Pilih -", "User", "Admin"};
	
	// DATABASE
	public static final String USERNAME = "root";
	public static final String PASSWORD = "";
	public static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://localhost:3306/cashier";
	public static final String ERROR_GET_DATA = "Get Data Failed";
}
