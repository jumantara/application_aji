package com.application.model;

public class RegistrationMod {
	private Long id_cust;
	private Long id_user;
	private String username;
	private String handphone;
	private String alamat;
	private String password;
	private String level;
	
	public Long getId_cust() {
		return id_cust;
	}
	public void setId_cust(Long id_cust) {
		this.id_cust = id_cust;
	}
	public Long getId_user() {
		return id_user;
	}
	public void setId_user(Long id_user) {
		this.id_user = id_user;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getHandphone() {
		return handphone;
	}
	public void setHandphone(String handphone) {
		this.handphone = handphone;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
}
