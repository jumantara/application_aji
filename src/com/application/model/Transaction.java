package com.application.model;

public class Transaction {
	private Long id_order;
	private Long id_product;
	private Long id_cust;
	private Long qty;
	private String amount;
	public Long getId_order() {
		return id_order;
	}
	public void setId_order(Long id_order) {
		this.id_order = id_order;
	}
	public Long getId_product() {
		return id_product;
	}
	public void setId_product(Long id_product) {
		this.id_product = id_product;
	}
	public Long getId_cust() {
		return id_cust;
	}
	public void setId_cust(Long id_cust) {
		this.id_cust = id_cust;
	}
	public Long getQty() {
		return qty;
	}
	public void setQty(Long qty) {
		this.qty = qty;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
