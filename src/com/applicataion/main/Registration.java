package com.applicataion.main;


import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.application.dao.RegistrationDao;
import com.application.model.RegistrationMod;
import com.application.util.Constant;
import javax.swing.JCheckBox;


public class Registration extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JTextField txtHandphone;
	private JTextField txtAlamat;
	private JPasswordField txtPassword;
	private JPasswordField txtConfirmpass;
	private Object cbx;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registration frame = new Registration();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Registration() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle (Constant.TITTLE_APP);
		setBounds(100, 100, Constant.WIDTH_PANEL, Constant.HEIGHT_PANEL);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblUsername.setBounds(142, 60, 58, 14);
		contentPane.add(lblUsername);
		
		JLabel lblHandphone = new JLabel("Handphone");
		lblHandphone.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHandphone.setBounds(142, 85, 64, 14);
		contentPane.add(lblHandphone);
		
		JLabel lblAlamat = new JLabel("Alamat");
		lblAlamat.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAlamat.setBounds(142, 110, 46, 14);
		contentPane.add(lblAlamat);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPassword.setBounds(142, 210, 64, 14);
		contentPane.add(lblPassword);
		
		JLabel lblConfirmPassword = new JLabel("Confirm Password");
		lblConfirmPassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblConfirmPassword.setBounds(142, 235, 101, 14);
		contentPane.add(lblConfirmPassword);
		
		txtUsername = new JTextField();
		txtUsername.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtUsername.setBounds(250, 57, 139, 20);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		txtHandphone = new JTextField();
		txtHandphone.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtHandphone.setBounds(250, 82, 139, 20);
		contentPane.add(txtHandphone);
		txtHandphone.setColumns(10);
		
		txtAlamat = new JTextField();
		txtAlamat.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtAlamat.setBounds(250, 107, 139, 52);
		contentPane.add(txtAlamat);
		txtAlamat.setColumns(10);
		
		
		
		JComboBox cbx = new JComboBox();
		cbx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cbx =(JComboBox) e.getSource();
				Object object = cbx.getSelectedItem();
				
				if (object.toString().equalsIgnoreCase(Constant.LEVEL_TYPE[0])) {
					
				}else if(object.toString().equalsIgnoreCase(Constant.LEVEL_TYPE[1])) {
					
				}else if(object.toString().equalsIgnoreCase(Constant.LEVEL_TYPE[2])) {
					
				}
			}
		});
		cbx.setFont(new Font("Tahoma", Font.BOLD, 11));
		cbx.setModel(new DefaultComboBoxModel(new String[] {"- Pilih -", "Admin", "User"}));
		cbx.setBounds(250, 170, 139, 24);
		contentPane.add(cbx);
		
		txtPassword = new JPasswordField();
		txtPassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtPassword.setBounds(250, 207, 139, 20);
		contentPane.add(txtPassword);
		
		txtConfirmpass = new JPasswordField();
		txtConfirmpass.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent e) {
//				String Password = new String (txtPassword.getPassword());
//				String retypePassword = new String (txtConfirmpass.getPassword());
//				
//				if (Password.equals(retypePassword)) {
//					
//				}else if (!retypePassword.equals(retypePassword)) {
//					JOptionPane.showMessageDialog(null, "Password tidak sama, silahkan ulangi kembali", "Failed", JOptionPane.ERROR_MESSAGE);
//				}
			}
		});
		txtConfirmpass.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtConfirmpass.setBounds(250, 232, 139, 20);
		contentPane.add(txtConfirmpass);
		
		JLabel lblStatus = new JLabel("Level");
		lblStatus.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblStatus.setBounds(142, 172, 46, 14);
		contentPane.add(lblStatus);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Password = new String (txtPassword.getPassword());
				String retypePassword = new String (txtConfirmpass.getPassword());
				
				if (!retypePassword.equals(Password)) {
					JOptionPane.showMessageDialog(null, "Password tidak sama, silahkan ulangi kembali", "Failed", JOptionPane.ERROR_MESSAGE);
			} else if
					(validateConfirm()) {
						
						RegistrationDao registrationDao = new RegistrationDao();
						RegistrationMod registrationMod = (RegistrationMod) registrationDao.getDataByVisibleCode(txtUsername.getText());
						if (registrationMod == null) {
							registrationMod = new RegistrationMod();
							registrationMod.setUsername(txtUsername.getText());
						}
						registrationMod.setHandphone(txtHandphone.getText());
						registrationMod.setAlamat(txtAlamat.getText());
						
						String levelType = cbx.getSelectedItem().toString();
						
						registrationMod.setPassword(txtPassword.getToolTipText());
						registrationDao.saveOrUpdate(registrationMod);
						
						JOptionPane.showMessageDialog(null, "Data Succesfuly", "success", JOptionPane.INFORMATION_MESSAGE);
						cbx.setSelectedIndex(0);
						setFormEmpty();
					} else {
						JOptionPane.showMessageDialog(null, "Data Failed", "Failed", JOptionPane.ERROR_MESSAGE);
					}	
				}
			}
		);
		btnSave.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSave.setBounds(219, 280, 74, 35);
		contentPane.add(btnSave);
		
		JCheckBox cxPassword = new JCheckBox("Cek Password");
		cxPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cxPassword.isSelected()) {
					txtPassword.setEchoChar((char)0);
				}
				else {
					txtPassword.setEchoChar('*');
				}
			}
		});
		cxPassword.setBounds(404, 206, 97, 23);
		contentPane.add(cxPassword);
		
		JCheckBox cxConfirm = new JCheckBox("Cek Konfirmasi Password");
		cxConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cxConfirm.isSelected()) {
					txtConfirmpass.setEchoChar((char)0);
				}
				else {
					txtConfirmpass.setEchoChar('*');
				}
			}
		});
		cxConfirm.setBounds(404, 235, 152, 23);
		contentPane.add(cxConfirm);
		
		
	}

	private boolean validateConfirm() {
		boolean isValid = true;
		JTextField jtextField[] = {txtUsername, txtHandphone, txtAlamat};
		JPasswordField jpasswordField[] = {txtPassword, txtConfirmpass};
		
		for (int i = 0; i < jtextField.length; i++) {
			if (jtextField[i].getText().equalsIgnoreCase("")) {
				isValid = false;
				break;
			}
		}
		
		for (int i = 0; i < jpasswordField.length; i++) {
			if (jpasswordField[i].getPassword().clone() != null) {
				isValid = false;
				break;
			}
		}
		
		if (!isValid) {
			JOptionPane.showConfirmDialog(null, "Form must be completed", "Failed", JOptionPane.ERROR_MESSAGE);
		}
		return isValid;
	}
	private void setFormEmpty() {
		JTextField jtextField[] = {txtUsername, txtHandphone, txtAlamat};
		JPasswordField jpasswordField[] = {txtPassword, txtConfirmpass};
		
		for (int i = 0; i < jtextField.length; i++) {
			jtextField[i].setText("");
		}
		for (int i = 0; i < jpasswordField.length; i++) {
			jpasswordField[i].setEchoChar((char) 0);
		}
	}
}

