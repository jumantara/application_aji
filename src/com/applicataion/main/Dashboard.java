package com.applicataion.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.application.panel.HistoryPanel;
import com.application.panel.ProductPanel;
import com.application.panel.TransactionPanel;
import com.application.util.Constant;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Dashboard extends JFrame {

	private JPanel masterPane;
	private JPanel panelHistory;
	private JPanel panelProduct;
	private JPanel panelDashboard;
	private JPanel panelTransaction;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dashboard frame = new Dashboard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dashboard() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, Constant.WIDTH_PANEL, Constant.HEIGHT_PANEL);
		setTitle (Constant.TITTLE_APP);
		
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menuHome = new JMenu("Home");
		menuHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setAllPanelInvisible();
				panelDashboard.setVisible(true);
			}
		});
		menuBar.add(menuHome);
		
		JMenu parentTransaction = new JMenu("Transaction");
		menuBar.add(parentTransaction);
		
		JMenuItem menuShopping = new JMenuItem("Shopping");
		menuShopping.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadTransactionPanel();
			}
		});
		parentTransaction.add(menuShopping);
		
		JMenuItem menuHistory = new JMenuItem("History");
		menuHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadHistoryPanel();
			}
		});
		parentTransaction.add(menuHistory);
		
		JMenu parentProduct = new JMenu("Product");
		menuBar.add(parentProduct);
		
		JMenuItem addProduct = new JMenuItem("Add Product");
		addProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadProductPanel();
			}
		});
		parentProduct.add(addProduct);
		masterPane = new JPanel();
		masterPane.setBackground(Color.GRAY);
		masterPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(masterPane);
		masterPane.setLayout(null);
		
		panelDashboard = new JPanel();
		panelDashboard.setBackground(Color.GRAY);
		panelDashboard.setBounds(0, 0, Constant.WIDTH_PANEL, Constant.HEIGHT_PANEL);
		masterPane.add(panelDashboard);
		panelDashboard.setVisible(true);
		panelDashboard.setLayout(null);
		
		JTextPane titleDashboard = new JTextPane();
		titleDashboard.setBounds(203, 148, 267, 70);
		titleDashboard.setEditable(false);
		titleDashboard.setText("System Kasir");
		titleDashboard.setForeground(Color.BLACK);
		titleDashboard.setFont(new Font("Dialog", Font.BOLD, 40));
		titleDashboard.setBackground(Color.GRAY);
		panelDashboard.add(titleDashboard);
		
		
		panelHistory = new HistoryPanel();
		masterPane.add(panelHistory);
		
		panelProduct = new ProductPanel();
		masterPane.add(panelProduct);
		
		
		panelTransaction = new TransactionPanel();
		masterPane.add(panelTransaction);
	}

	public void loadProductPanel() {
		setAllPanelInvisible();
		this.panelProduct.setVisible(true);
		
	}

	public void loadHistoryPanel() {
		setAllPanelInvisible();
		this.panelHistory.setVisible(true);
		
	}

	public void loadTransactionPanel() {
		setAllPanelInvisible();
		this.panelTransaction.setVisible(true);
		
	}

	public void setAllPanelInvisible() {
		this.panelProduct.setVisible(false);
		this.panelDashboard.setVisible(false);
		this.panelHistory.setVisible(false);
		this.panelTransaction.setVisible(false);
		
	}
}
