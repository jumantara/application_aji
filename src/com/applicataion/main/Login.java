package com.applicataion.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.application.util.Constant;
import javax.swing.JFormattedTextField;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		getContentPane().setBackground(new Color(255, 204, 0));
		setTitle (Constant.TITTLE_APP);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, Constant.WIDTH_PANEL, Constant.HEIGHT_PANEL);
		getContentPane().setLayout(null);
		
		JTextPane txtPane = new JTextPane();
		txtPane.setBackground(new Color(255, 204, 0));
		txtPane.setEditable(false);
		txtPane.setFont(new Font("OCR A Extended", Font.BOLD, 20));
		txtPane.setText("Welcome To Tara House");
		txtPane.setBounds(172, 36, 259, 28);
		getContentPane().add(txtPane);
		
		JTextPane txtpnLogin = new JTextPane();
		txtpnLogin.setBackground(new Color(255, 204, 0));
		txtpnLogin.setEditable(false);
		txtpnLogin.setFont(new Font("Arial Black", Font.BOLD, 18));
		txtpnLogin.setText("Login ");
		txtpnLogin.setBounds(272, 75, 67, 32);
		getContentPane().add(txtpnLogin);
		
		JLabel lblNewLabel = new JLabel("Username");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setBounds(172, 140, 83, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Arial", Font.BOLD, 15));
		lblPassword.setBounds(172, 178, 83, 14);
		getContentPane().add(lblPassword);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(272, 138, 159, 20);
		getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String password = txtPassword.getText();
				String username = txtUsername.getText();
				
				if (username.contains("tara") && password.contains("house")) {
					JOptionPane.showMessageDialog(null, "Data Succesfuly", Constant.TITTLE_APP, JOptionPane.INFORMATION_MESSAGE);
					txtUsername.setText(null);
					txtPassword.setText(null);
				}
				else 
				{
					JOptionPane.showMessageDialog(null, "Data Invalid", Constant.TITTLE_APP, JOptionPane.ERROR_MESSAGE);
					txtUsername.setText(null);
					txtPassword.setText(null);	
				}
			}
		});
		btnLogin.setBackground(Color.LIGHT_GRAY);
		btnLogin.setFont(new Font("Arial", Font.BOLD, 13));
		btnLogin.setBounds(258, 229, 75, 23);
		getContentPane().add(btnLogin);
		
		JButton btnRegistration = new JButton("Create your Account Now");
		btnRegistration.setBackground(Color.LIGHT_GRAY);
		btnRegistration.setFont(new Font("Arial", Font.BOLD, 13));
		btnRegistration.setBounds(187, 263, 210, 23);
		getContentPane().add(btnRegistration);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(new Color(0, 51, 0));
		separator.setBounds(74, 207, 457, 2);
		getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(new Color(0, 51, 0));
		separator_1.setBounds(74, 308, 457, 2);
		getContentPane().add(separator_1);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(272, 176, 159, 20);
		getContentPane().add(txtPassword);
		
	}
}
